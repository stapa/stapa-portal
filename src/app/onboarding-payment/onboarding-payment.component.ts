import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { take } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


interface VerificationResult {
  checkout_status: string,
  payment_status: string
}

interface SubscriptionOption {
  id: number,
  name: string,
  price: number,
  valid_for_months: number,
  valid_from: string,
  valid_to: string
}

interface PendingCheckout {
  url: string,
  subscription_option: SubscriptionOption
}

interface ConfirmedSubscription {
  id: number,
  active: boolean,
  valid_from: string,
  valid_to: string,
  remaining_days: number
}

interface PaymentState {
  confirmed: ConfirmedSubscription[],
  available: SubscriptionOption[],
  pending_checkouts: PendingCheckout[],
}

@Component({
  selector: 'app-onboarding-payment',
  templateUrl: './onboarding-payment.component.html',
  styleUrls: ['./onboarding-payment.component.scss']
})
export class OnboardingPaymentComponent implements OnInit {
  paymentState!: PaymentState;
  subOptionPriceMap: { [key: number]: number } = {};
  paymentUrlLoading = false;
  paymentSuccessRedirect = false;
  paymentVerified!: boolean;
  checkoutStarted = false;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    console.log('[OnboardingPayment] init')
    this.route.queryParams.pipe(take(1)).subscribe(params => {
      if (params['payment'] == "success") {
        this.paymentSuccessRedirect = true;
        this.verifyPayment();
      } else {
        this.paymentSuccessRedirect = false;
        this.updateState();
      }
      this.router.navigate([], { relativeTo: this.route, queryParams: {}})
    });
  }

  updateState() {
    console.log('[OnboardingPayment] Fetching /onboarding/payment')
    this.userService.apiGet<PaymentState>(
      '/onboarding/payment'
    ).subscribe((state) => {
      this.paymentState = state;
      this.paymentState.available.forEach(element => {
        this.subOptionPriceMap[element.id] = element.price;
      });
      console.log('[OnboardingPayment] PaymentState fetched.')
      if (state.pending_checkouts.length > 0) {
        this.verifyPayment();
      }
    });
  }

  recoverPayment() {
    window.location.href = this.paymentState.pending_checkouts[0].url;
  };

  goToCheckout(option_id: number) {
    this.checkoutStarted = true;
    this.paymentUrlLoading = true;
    console.log(`[OnboardingPayment] Subscription option ${option_id} selected.`)
    this.userService.apiPost<any>(
      '/onboarding/payment/subscription_option/' + option_id, null
    ).subscribe((response) => {
      console.log(`[OnboardingPayment] Redirecting to checkout page: ${response.payment_link}`)
      window.location.href = response.payment_link;
    })
  }

  verifyPayment() {
    console.log('[OnboardingPayment] Verifying payment...');
    this.userService.apiGet<VerificationResult>('/onboarding/payment/verify')
    .subscribe({
      next: (res) => {
        if (res.payment_status == 'paid') {
          // TRANSACTION COMPLETED _ AMOUNT CAPTURED
          this.paymentVerified = true;
          this.updateState();
          
          // also refresh user to enable new options in nav menu
          this.userService.refresh();
        } else if (res.checkout_status == 'complete') {
          // TRANSACTION COMPLETED _ AMOUNT STILL UNCAPTURED
          setTimeout(() => {this.verifyPayment();}, 5000);
        } else if (res.checkout_status == 'open') {
          // TRANSACTION STILL OPEN
          this.paymentVerified = false;
          this.updateState();
        }
      },
      error: (err: HttpErrorResponse) => {
        if (err.status == 404) {
          // In case of free membership there is no payment checkout object,
          // so the server returns 404. Handle that case.
          this.paymentVerified = true;
          this.updateState();
        } else {
          throw err;
        }
      }
    });
  }

}
