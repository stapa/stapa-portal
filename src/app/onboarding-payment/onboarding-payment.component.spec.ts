import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingPaymentComponent } from './onboarding-payment.component';

describe('OnboardingPaymentComponent', () => {
  let component: OnboardingPaymentComponent;
  let fixture: ComponentFixture<OnboardingPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnboardingPaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnboardingPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
