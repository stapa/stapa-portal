import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  userInfo$ = this.userService.userInfo$;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userService: UserService
  ) {}

  logout() {
    this.userService.apiPost<{logout_url: string}>('/auth/logout', {}).subscribe({
      next: (res) => {
        window.location.href = res.logout_url;
      }
    });

  }



}
