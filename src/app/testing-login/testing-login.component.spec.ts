import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingLoginComponent } from './testing-login.component';

describe('TestingLoginComponent', () => {
  let component: TestingLoginComponent;
  let fixture: ComponentFixture<TestingLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestingLoginComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestingLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
