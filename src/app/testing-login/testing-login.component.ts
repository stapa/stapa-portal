import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Location } from '@angular/common'

import { UserService } from '../user.service';

@Component({
  selector: 'app-testing-login',
  templateUrl: './testing-login.component.html',
  styleUrls: ['./testing-login.component.scss']
})
export class TestingLoginComponent implements OnInit {
  constructor(private userService: UserService, private location: Location) { }

  ngOnInit() {
    this.location.back();
  }

}
