import { Component, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroupDirective, NgForm, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog'
import { UserService } from '../user.service';
import { ErrorStateMatcher, MatOptionSelectionChange } from '@angular/material/core';

interface PasteurPosition {
  id: number,
  description: string
}


class BadgeFormErrorStateMatcher implements ErrorStateMatcher {
  constructor(private errorCode: string) { }
  isErrorState(control: FormControl, form: FormGroupDirective | NgForm): boolean {
    return (control.touched || control.dirty) && (control.invalid || form.hasError(this.errorCode));
  }
}

@Component({
  selector: 'app-onboarding-badge',
  templateUrl: './onboarding-badge.component.html',
  styleUrls: ['./onboarding-badge.component.scss']
})
export class OnboardingBadgeComponent {
  @Output() completed = new EventEmitter<null>();
  pasteurPositions: PasteurPosition[] = [];

  isBadgeNumber(control: AbstractControl): ValidationErrors | null {
    let value = control.value;
    if (value) {
      return (value.length >= 3 && /^\d+$/.test(value)) ? null : {'notBadgeNumber': true};
    } else {
      return null;
    }
  }

  requiredIfOtherPosition(control: AbstractControl): ValidationErrors | null {
    let value = control.value;
    if (value.pasteurPosition == 'PERSONALIZED' && value.pasteurPositionOther.length === 0) {
      return {'missingPositionOther': true};
    } else {
      return null;
    }
  }

  badgeForm = this.fb.group({
    badge: new FormControl<string|null>(null, [Validators.required, this.isBadgeNumber]),
    acceptStatute: new FormControl<boolean>(false, Validators.requiredTrue),
    acceptGDPR: new FormControl<boolean>(false, Validators.requiredTrue),
    pasteurPosition: new FormControl<string|null>(null, [Validators.required]),
    pasteurPositionOther: new FormControl<string|null>(null)
  }, { validators: [this.requiredIfOtherPosition]});

  positionOtherStateMatcher = new BadgeFormErrorStateMatcher('missingPositionOther');

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  onFileSelected(event: Event) {
    let file = (event.target as HTMLInputElement).files?.[0];
    if (file) {
      let formData = new FormData();
      formData.append("badge", file);
      this.userService.apiPost<any>(
        '/onboarding/badge/scan',
        formData
      ).subscribe({
        'next': (response) => {
          console.log('[OnboardingBadge]: image upload result: ' + response.matricule)
          if (response.matricule) {
            this.badgeForm.patchValue({badge: response.matricule});
          } else {
            this.uploadError();
          }
        },
        'error': (error) => {
          console.log('[OnboardingBadge]: image upload error');
          this.uploadError();
        }
      })
    }
  }

  submitForm() {
    console.log('[OnboardingBadge] updating /onboarding/badge');

    this.userService.apiPut(
      '/onboarding/badge',
      (this.badgeForm.value.pasteurPosition === "PERSONALIZED")
      ? {
        matricule: this.badgeForm.value.badge,
        pasteur_position_other: this.badgeForm.value.pasteurPositionOther
      }
      : {
        matricule: this.badgeForm.value.badge,
        pasteur_position_id: this.badgeForm.value.pasteurPosition
      }
    ).subscribe({
      'next': () => {
        console.log('[OnboardingBadge] /onboarding/badge updated');
        this.completed.emit();
      },
      'error': () => {
        console.log('[OnboardingBadge] ERROR while updating');
      }
    })
  }

  resetPasteurPosition(event: MatOptionSelectionChange) {
    if (event.isUserInput) {
      this.badgeForm.controls['pasteurPositionOther'].setValue('');
    }
  }

  ngOnInit() {
    console.log('[OnboardingBadge] fetching /onboarding/badge/positions');
    this.userService.apiGet<PasteurPosition[]>('/onboarding/badge/positions').subscribe({
      next: (response) => {
        this.pasteurPositions = response;
      }
    });
    console.log('[OnboardingBadge] fetching /onboarding/badge');
    this.userService.apiGet<any>('/onboarding/badge').subscribe({
      next: (response) => {
        this.badgeForm.patchValue({
          badge: response.matricule,
          pasteurPosition: response.pasteur_position_other ? "PERSONALIZED" : response.pasteur_position_id,
          pasteurPositionOther: response.pasteur_position_other
        });
        console.log('[OnboardingBadge] /onboarding/badge fetched')
      }
    });
  }

  uploadError() {
    this.dialog.open(DialogUploadError);
  }

  openStatute() {
    this.dialog.open(StatuteDialogComponent);
  }

  openPrivacy() {
    this.dialog.open(PrivacyDialogComponent);
  }
}


@Component({
  selector: 'statute-dialog-component',
  templateUrl: './statute-dialog-component.html',
})
export class StatuteDialogComponent {}


@Component({
  selector: 'privacy-dialog-component',
  templateUrl: './privacy-dialog-component.html',
})
export class PrivacyDialogComponent {}


@Component({
  selector: 'dialog-upload-error',
  templateUrl: './dialog-upload-error.html',
  styleUrls: ['./onboarding-badge.component.scss']
})
export class DialogUploadError {}
