import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingBadgeComponent } from './onboarding-badge.component';

describe('OnboardingBadgeComponent', () => {
  let component: OnboardingBadgeComponent;
  let fixture: ComponentFixture<OnboardingBadgeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnboardingBadgeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnboardingBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
