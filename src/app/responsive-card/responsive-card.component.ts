import { Component, Input } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';


@Component({
  selector: 'app-responsive-card',
  templateUrl: './responsive-card.component.html',
  styleUrls: ['./responsive-card.component.scss']
})
export class ResponsiveCardComponent {

  @Input() minWidth: number = 500; 

  smallScreen$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 800px)')
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(private breakpointObserver: BreakpointObserver) {}


}
