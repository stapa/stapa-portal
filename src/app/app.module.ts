import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatStepperModule } from '@angular/material/stepper'
import { OnboardingComponent } from './onboarding/onboarding.component';
import { HttpClientModule } from '@angular/common/http';
import { OnboardingAuthComponent } from './onboarding-auth/onboarding-auth.component';
import { TestingLoginComponent } from './testing-login/testing-login.component';
import { OnboardingContactComponent } from './onboarding-contact/onboarding-contact.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { OnboardingBadgeComponent, DialogUploadError, StatuteDialogComponent, PrivacyDialogComponent } from './onboarding-badge/onboarding-badge.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatDialogModule } from '@angular/material/dialog';
import { OnboardingPaymentComponent } from './onboarding-payment/onboarding-payment.component';
import { ResponsiveCardComponent } from './responsive-card/responsive-card.component';
import { MembercardComponent } from './membercard/membercard.component';
import { DrinkComponent } from './drink/drink.component';
import { SwipeCheckComponent } from './swipe-check/swipe-check.component';
import { IonicModule } from '@ionic/angular';

export const MY_FORMATS = {
  parse: {
      dateInput: 'DD/MM/YYYY'
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'YYYY'
  }
};
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    OnboardingComponent,
    OnboardingAuthComponent,
    TestingLoginComponent,
    OnboardingContactComponent,
    OnboardingBadgeComponent,
    StatuteDialogComponent,
    DialogUploadError,
    PrivacyDialogComponent,
    OnboardingPaymentComponent,
    ResponsiveCardComponent,
    MembercardComponent,
    SwipeCheckComponent,
    DrinkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatStepperModule,
    HttpClientModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    NgxMatIntlTelInputComponent,
    MatDatepickerModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    IonicModule.forRoot(),
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'fr' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
