import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/cdk/stepper';
import { Observable, firstValueFrom, fromEvent, takeUntil } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatStep, MatStepper } from '@angular/material/stepper';

import { UserService } from '../user.service';
import { OnboardingAuthComponent } from '../onboarding-auth/onboarding-auth.component';
import { OnboardingContactComponent } from '../onboarding-contact/onboarding-contact.component';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {
  username!: string;
  paymentCallback!: boolean;
  
  stepperOrientation: Observable<StepperOrientation>;
  @ViewChild(MatStepper) stepper!: MatStepper;
  @ViewChild('authStep') authStep!: MatStep;
  @ViewChild('contactStep') contactStep!: MatStep;
  @ViewChild('badgeStep') badgeStep!: MatStep;
  @ViewChild('paymentStep') paymentStep!: MatStep;


  ngOnInit() {
      // disable browser back/forward buttons
      history.pushState(null, '');
      fromEvent(window, 'popstate').subscribe((_) => {
        history.pushState(null, '');
      });
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userService: UserService,
    private route: ActivatedRoute,
    private location: LocationStrategy
  ) {

    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({matches}) => (matches ? 'horizontal' : 'vertical')));


    this.userService.userInfo$.subscribe((userInfo) => {
      if (userInfo.username) {
        this.username = userInfo.username;
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['payment'] == "success") {
        this.paymentCallback = true;
      } else {
        this.paymentCallback = false;
      }
    });
  }

  

  onAuthCompleted() {
    console.log("[Onboarding] Auth completed");
    setTimeout(() => {
      this.authStep.completed = true;
      this.stepper.next();
    }, 100);
  }

  onContactCompleted() {
    console.log("[Onboarding] Contact form completed");
    setTimeout(() => {
      this.contactStep.completed = true;
      this.stepper.next();
    }, 100);
  }

  onBadgeCompleted() {
    console.log("[Onboarding] Badge form completed");
    setTimeout(() => {
      this.badgeStep.completed = true;
      this.stepper.next();
    })
  }

  onStepChange(event: any) {
    console.log("[Onboarding] Step changed to: " + event.selectedIndex)
  }
}
