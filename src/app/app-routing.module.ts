import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MembercardComponent } from './membercard/membercard.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { DrinkComponent } from './drink/drink.component';

const routes: Routes = [
  { path: 'onboarding', component: OnboardingComponent },
  { path: 'membercard', component: MembercardComponent },
  { path: 'drink', component: DrinkComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
