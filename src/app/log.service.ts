import { Injectable } from '@angular/core';

import { LogPublisher } from './log-publisher';
import { LogPublisherService } from './log-publisher.service';

export enum LogLevel {
  All = 0,
  Debug = 1,
  Info = 2,
  Warn = 3,
  Error = 4,
  Fatal = 5,
  Off = 6
}

@Injectable({
  providedIn: 'root'
})


export class LogEntry {
  clientDate: Date = new Date();
  message: string = "";
  level: LogLevel = LogLevel.Debug;
  extraInfo: any[] = [];

  toString(): string {
    let value: string = "";
    value += "Type: " + LogLevel[this.level];
    value += " - Message: " + this.message;
    if (this.extraInfo.length) {
      value += " - Extra Info: " + this.formatParams(this.extraInfo);
    }
    return value;
  }

  private formatParams(params: any[]): string {
    let value: string = params.join(",");
    if (params.some(p => typeof p == "object")) {
      value = "";
      for (let item of params) {
        value += JSON.stringify(item) + ",";
      }
    }
    return value;
  }
}
export class LogService {
  level: LogLevel = LogLevel.All;
  publishers: LogPublisher[];

  constructor(private publisherService: LogPublisherService) {
    this.publishers = this.publisherService.publishers;
  }

  debug(message: string, ...optionalParams: any[]) {
    this.writeToLog(message, LogLevel.Debug, optionalParams);
  }

  info(message: string, ...optionalParams: any[]) {
    this.writeToLog(message, LogLevel.Info, optionalParams);
  }

  warn(message: string, ...optionalParams: any[]) {
    this.writeToLog(message, LogLevel.Warn, optionalParams);
  }

  error(message: string, ...optionalParams: any[]) {
    this.writeToLog(message, LogLevel.Error, optionalParams);
  }

  fatal(message: string, ...optionalParams: any[]) {
    this.writeToLog(message, LogLevel.Fatal, optionalParams);
  }

  private shouldLog(level: LogLevel): boolean {
    let ret: boolean = false;
    if ((level >= this.level && level !== LogLevel.Off) || this.level === LogLevel.All) {
        ret = true;
    }
    return ret;
  }

  private writeToLog(message: string, level: LogLevel, params: any[]) {
    if (this.shouldLog(level)) {
      let entry: LogEntry = new LogEntry();
      entry.message = message;
      entry.level = level;
      entry.extraInfo = params;
      for (let logger of this.publishers) {
        logger.log(entry).subscribe(response => console.log(response));
      }
    }
  }
}