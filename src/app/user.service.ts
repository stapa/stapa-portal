import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable, ReplaySubject } from 'rxjs';
import { ReadPropExpr } from '@angular/compiler';



export interface UserInfo {
  username: string;
  name: string;
  has_card: boolean;
}


export interface LoginInfo {
  login_url: string;
}




@Injectable({
  providedIn: 'root'
})
export class UserService {
  // backendUrl: string = "http://localhost:5000";
  // backendUrl: string = "https://api-test.stapa.fr";
  backendUrl: string = "https://api.stapa.fr";
  loggedIn$ = new ReplaySubject<boolean>(1);
  userInfo$ = new ReplaySubject<UserInfo>(1);
  userInfo!: UserInfo;
  loginUrl$ = new ReplaySubject<string>(1);

  constructor(private httpClient: HttpClient) {
    console.log("[UserService] started")
    this.refresh()
  }


  apiGet<T>(endpoint: string) {
    return this.httpClient.get<T>(
      this.backendUrl + endpoint,
      { withCredentials: true }
    );
  }

  apiPut<T>(endpoint: string, data: any) {
    return this.httpClient.put<T>(
      this.backendUrl + endpoint,
      data,
      { withCredentials: true}
    );
  }

  apiPost<T>(endpoint: string, data: any) {
    return this.httpClient.post<T>(
      this.backendUrl + endpoint,
      data,
      { withCredentials: true}
    );
  }

  refresh() {
    console.log("[UserService] fetching '/me'")
    this.getUserInfo()
  }

  getUserInfo() {
    this.apiGet<UserInfo>('/me').subscribe({
      next: (response: UserInfo) => {
        console.log("[UserService] '/me' fetched");
        this.loggedIn$.next(true);
        this.userInfo$.next(response);
        this.userInfo = response
      },
      error: (error: HttpErrorResponse) => {
        if (error.status == 401) {
          console.log("[UserService] unauthorized");
          this.loggedIn$.next(false);
          // this.getLoginUrl();
        }
        // TODO: handle unexpeceted errors
      }
    });
  }

  getLoginUrl(redirectUrl: string) {
    return this.apiPost<LoginInfo>(
      '/auth/login',
      { redirect_url: redirectUrl },
    )
  }

}
