import { TestBed } from '@angular/core/testing';

import { StapaApiService } from './stapa-api.service';

describe('StapaApiService', () => {
  let service: StapaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StapaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
