import { Component, inject } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-drink',
  templateUrl: './drink.component.html',
  styleUrls: ['./drink.component.scss']
})
export class DrinkComponent {
  user = inject(UserService);
  eligible$ = this.user.apiGet<{eligible: boolean}>('/wallet/self/free_drink')


  onReclaim() {
    this.user.apiPost<{executed: boolean}>('/wallet/self/free_drink', {}).subscribe({
      next: (executed) => {
        if(executed) {
          window.alert('Free drink redeemed! Do not close this message if you haven\'t shown your phone to the organizers yet.');
          this.eligible$ = this.user.apiGet<{eligible: boolean}>('/wallet/self/free_drink');
        } else {
          window.alert('ERROR: SOMETHING WENT WRONG, it seems that you already had your free drink');
        }
      }
    })
  }
}
