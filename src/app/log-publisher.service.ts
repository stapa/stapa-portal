import { Injectable } from '@angular/core';

import { LogPublisher, LogConsole } from './log-publisher';

@Injectable({
  providedIn: 'root'
})
export class LogPublisherService {
  publishers: LogPublisher[] = [];

  constructor() {
    this.buildPublishers();
  }

  buildPublishers(): void {
    this.publishers.push(new LogConsole());
  }
}
