import { BreakpointObserver } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

import { UserService } from '../user.service';

@Component({
  selector: 'app-onboarding-auth',
  templateUrl: './onboarding-auth.component.html',
  styleUrls: ['./onboarding-auth.component.scss']
})
export class OnboardingAuthComponent {
  loggedIn!: boolean;
  loginUrl!: string;
  valid: boolean = false;
  @Output() completed = new EventEmitter<null>();


  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.loggedIn$.subscribe((loggedIn: boolean) => {
      console.log("[OnboardingAuth] login status changed: " + loggedIn);
      this.loggedIn = loggedIn;
      if (loggedIn) {
        this.verifyLogin();
      } else {
        this.getLoginUrl();
      }
    });
  }

  getLoginUrl() {
    this.userService.getLoginUrl(window.location.href).subscribe({
      next: (response) => {
        this.loginUrl = response.login_url;
      }
    })
  }

  setLocation(targetUrl: string) {
    location.href = targetUrl;
  }

  verifyLogin() {
    // Extra checks to add (valid tenant etc...)
    this.valid = this.loggedIn;
    if (this.valid) {
      console.log("[OnboardingAuth] Auth completed. Sending signal")
      this.completed.emit();
    } else {
      console.log("[OnboardingAuth] Auth required")
    }
  }

}
