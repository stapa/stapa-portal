import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingAuthComponent } from './onboarding-auth.component';

describe('OnboardingAuthComponent', () => {
  let component: OnboardingAuthComponent;
  let fixture: ComponentFixture<OnboardingAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnboardingAuthComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnboardingAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
