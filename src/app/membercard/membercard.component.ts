import { Component } from '@angular/core';
import { UserService } from '../user.service';

interface MembercardInfo {
  id: number,
  display_name: string,
  pronouns: string,
  valid_to: string
}

@Component({
  selector: 'app-membercard',
  templateUrl: './membercard.component.html',
  styleUrls: ['./membercard.component.scss']
})
export class MembercardComponent {
  constructor(private userSerive: UserService) {};

  membercardInfo$ = this.userSerive.apiGet<MembercardInfo>('/membercard');
  
}
