import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwipeCheckComponent } from './swipe-check.component';

describe('SwipeCheckComponent', () => {
  let component: SwipeCheckComponent;
  let fixture: ComponentFixture<SwipeCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwipeCheckComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SwipeCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
