import { Component, ElementRef, EventEmitter, Input, NgZone, Output, ViewChild, inject } from '@angular/core';
import { Animation, AnimationController, Gesture, GestureController, GestureDetail } from '@ionic/angular';

@Component({
  selector: 'app-swipe-check',
  templateUrl: './swipe-check.component.html',
  styleUrls: ['./swipe-check.component.scss']
})
export class SwipeCheckComponent {
@Input() text = "Swipe to confirm";
@Input() iconName = "cash-outline";
@Output() onVerify = new EventEmitter<void>();
@ViewChild('swipeButton', { read: ElementRef }) swipeButton!: ElementRef<HTMLElement>;
@ViewChild('swipeTrack', { read: ElementRef }) swipeTrack!: ElementRef<HTMLElement>;

private animationCtrl = inject(AnimationController);
private gestureCtrl = inject(GestureController);
private zone = inject(NgZone);

private animation!: Animation;
private gesture!: Gesture;
private started = false;
private initialStep = 0;
private resizeObserver!: ResizeObserver;

private maxTranslate!: number;

ngOnDestroy() {
  this.resizeObserver.disconnect();
}

private onResize() {
  this.maxTranslate = this.swipeTrack.nativeElement.clientWidth - 60;

  if(this.animation) {
    this.animation.destroy();
  }
  this.animation = this.animationCtrl
    .create()
    .addElement(this.swipeButton.nativeElement)
    .duration(300)
    .fromTo('transform', 'translateX(0)', `translateX(${this.maxTranslate}px)`);
}

private onMove(ev: GestureDetail) {
  if(!this.started) {
    this.animation.progressStart();
    this.started = true;
  }

  this.animation.progressStep(this.getStep(ev));
}

private onEnd(ev: GestureDetail) {
  if (!this.started) {
    return;
  }

  this.gesture.enable(false);

  const step = this.getStep(ev);
  const shouldComplete = step > 0.99;

  this.animation.progressEnd(shouldComplete ? 1 : 0, step).onFinish(() => {
    this.gesture.enable(true);
  });

  this.initialStep = shouldComplete ? this.maxTranslate : 0;
  this.started = false;

  if (shouldComplete) {
    console.log('[Component: SwipeVerify] Confirmed with swipe gesture')
    this.zone.run(() => { this.onVerify.emit(); });
  }
}

private clamp(min: number, n: number, max: number) {
  return Math.max(min, Math.min(n, max));
}

private getStep(ev: GestureDetail) {
  const delta = this.initialStep + ev.deltaX;
  return this.clamp(0, delta / this.maxTranslate, 1);
}

ngAfterViewInit() {
  this.resizeObserver = new ResizeObserver(() => { this.onResize(); });
  this.resizeObserver.observe(this.swipeTrack.nativeElement);
  this.onResize();

  const gesture = (this.gesture = this.gestureCtrl.create({
    el: this.swipeButton.nativeElement,
    threshold: 0,
    gestureName: 'button-drag',
    onMove: (ev) => this.onMove(ev),
    onEnd: (ev) => this.onEnd(ev),
  }));

  gesture.enable(true);
}
}