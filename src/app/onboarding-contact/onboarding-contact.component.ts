import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, map, shareReplay } from 'rxjs';
import { Output, EventEmitter } from '@angular/core';

import { UserService } from '../user.service';
import { formatDate } from '@angular/common';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';
import { MatOptionSelectionChange } from '@angular/material/core';


function shuffle(array: Array<any>): Array<any> {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

interface OnboardingContactData {
  birthdate: string | null,
  email_address: string | null,
  news_consent: boolean | null,
  phone_number: string | null,
  pronouns: string | null,
  status: string
}

@Component({
  selector: 'app-onboarding-contact',
  templateUrl: './onboarding-contact.component.html',
  styleUrls: ['./onboarding-contact.component.scss'],
})
export class OnboardingContactComponent implements OnInit {
  @Output() completed = new EventEmitter<null>();
  
  basicPronouns = shuffle([
    'they/them',
    'she/her',
    'he/him'
  ])

  notPasteur(control: AbstractControl): ValidationErrors | null {
    if (control.value) {
      return control.value.endsWith('@pasteur.fr') ? {'isPasteur': true} : null;
    } else {
      return null;
    }
  }
  contactForm = this.fb.group({
    usePronouns: new FormControl<boolean>(false),
    pronouns: new FormControl<string[]|null>(null),
    personalizedPronouns: new FormControl<string|null>(null),
    birthdate: new FormControl<string|null>(null, Validators.required),
    emailAddress: new FormControl<string|null>(null, [Validators.required, this.notPasteur]),
    phoneNumber: new FormControl<string|null>(null),
    newsConsent: new FormControl<boolean>(false),
    joinDL: new FormControl<boolean>(false),
    joinTeams: new FormControl<boolean>(false)
  });
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(
    Breakpoints.Handset
  ).pipe(
      map(result => result.matches),
      shareReplay()
  );

  constructor(
    private fb: FormBuilder,
    private breakpointObserver: BreakpointObserver,
    private userService: UserService
  ) {}

  populateForm(response: OnboardingContactData) {
    if (response.pronouns) {
      this.pronounsFromString(response.pronouns);
    }
    this.contactForm.patchValue({
      birthdate: response.birthdate,
      emailAddress: response.email_address,
      phoneNumber: response.phone_number,
      newsConsent: !!response.news_consent,
    });
  }
  ngOnInit() {
    console.log('[OnboardingContact] fetching /onboarding/contact');
    this.userService.apiGet<OnboardingContactData>('/onboarding/contact').subscribe({
      next: (response) => {
        this.populateForm(response);
        console.log('[OnboardingContact] /onboarding/contact fetched')
      }
    });
  }


  pronounsToString() {
    let pronouns = this.contactForm.value.pronouns;
    let personalizedPronouns = this.contactForm.value.personalizedPronouns;
    if (pronouns && pronouns.length > 0) {
      return pronouns.map(
        (value) => (value == 'PERSONALIZED') ? personalizedPronouns : value
      ).join(","); 
    } else {
      return null;
    }
  }

  pronounsFromString(pronounsString: string) {
    let allPronouns = pronounsString.split(',');
    let basicPronouns = allPronouns.filter((value) => this.basicPronouns.includes(value));
    let personalizedPronouns = allPronouns.filter((value) => !this.basicPronouns.includes(value));
    if (personalizedPronouns.length > 0) {
      basicPronouns.push('PERSONALIZED');
    }
    this.contactForm.patchValue({
      usePronouns: (allPronouns) ? true : false,
      pronouns: basicPronouns,
      personalizedPronouns: personalizedPronouns.join(",")
    });
  }
  submitForm() {
    let form = this.contactForm.value
    this.userService.apiPut(
      '/onboarding/contact',
      {
        birthdate: (form.birthdate) ? formatDate(form.birthdate, 'YYYY-MM-dd', 'en-US') : null,
        pronouns: this.pronounsToString(),
        phone_number: form.phoneNumber,
        email_address: form.emailAddress,
        news_consent: form.newsConsent,
        joinDL: form.joinDL,
        joinTeams: form.joinTeams
      }
    ).subscribe({
      next: (response) => {
        console.log('[OnboardingContact] submitted')
        this.completed.emit();
      }
    });
  }

  togglePronouns(event: MatCheckboxChange) {
    if (!event.checked) {
      this.contactForm.controls['pronouns'].setValue(null);
      this.contactForm.controls['personalizedPronouns'].setValue(null);
    }
  }

  resetPersonalizedPronouns(event: MatOptionSelectionChange) {
    if (event.isUserInput) {
      this.contactForm.controls['personalizedPronouns'].setValue('');
    }
  }

  getValue(event: Event) {
    return (event.target as HTMLInputElement).value;
  }
}
