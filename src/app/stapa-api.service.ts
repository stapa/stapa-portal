// credits to https://medium.com/@walid.bouguima/angular-generic-http-service-handle-your-backend-communication-like-a-pro-38422ca3ee2a
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class StapaApiService {
  constructor(private http: HttpClient) { }
  private apiBaseUrl = "https://freebeer.zolfa.nl"


  apiGet<T>(endpoint: string, options?: any): Observable<any> {
    return this.http.get<T>(this.apiBaseUrl + endpoint, options)
      .pipe(catchError(this.errorHandler))
  }

  apiPost<T>(endpoint: string, body?: any, options?: any): Observable<any> {
    return this.http.post<T>(this.apiBaseUrl + endpoint, options)
      .pipe(catchError(this.errorHandler))
  }

  apiPut<T>(endpoint: string, body?: any, options?: any): Observable<any> {
    return this.http.post<T>(this.apiBaseUrl + endpoint, options)
      .pipe(catchError(this.errorHandler))
  }


  errorHandler(error: HttpErrorResponse) {
    let errorMessage = "CommError: Unknown";
    if (error.error instanceof ErrorEvent) {
      errorMessage = `CommError: ${error.error.message}`;
    } else {
      errorMessage = `CommError: server ${error.status}\nMessage: ${error.message}`
    }
    window.alert(errorMessage)
    return throwError(() => new Error(errorMessage));
  }
}
