import { Observable, of, throwError } from 'rxjs';
import { LogEntry } from './log.service';

export abstract class LogPublisher {
    location?: string;
    abstract log(record: LogEntry):
    Observable<boolean>
}

export class LogConsole extends LogPublisher {
    log(entry: LogEntry): Observable<boolean> {
        // Log to console
        console.log(entry.toString());
        return of(true);
    }
}

export class LogWebApi extends LogPublisher {
    constructor(private http: Http) {
        super();
        this.location = "https://api.stapa.zolfa.nl/telemetry";
    }

    log(entry: LogEntry): Observable<boolean> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(this.location, entry, {headers: headers} )
            .map((response: Response) => response.json())
            .catch(this.handleErrors);
    }

    private handleErrors(error: any): Observable<any> {
        let errors: string[] = [];
        let msg: string = "";
        
        msg = "Status: " + error.status;
        msg += " - Status Text: " + error.statusText;
        if (error.json()) {
            msg += " - Exception Message: " + error.json().exceptionMessage;
        }
        errors.push(msg);
        
        console.error('An error occurred', errors);
        return throwError(() => new Error(errors));
    }
